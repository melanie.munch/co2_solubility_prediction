## Abstract

Carbon dioxide (CO2) is an important gas used in modified atmosphere packaging of non-respiring foods where it solubilizes into the aqueous and lipid phases of food and exerts an antimicrobial effect. Prediction of CO2 solubility within food is thus of paramount importance to anticipate its benefit on food preservation. In the present study, machine learning algorithms were applied on a set of 362 values of CO2 solubilities collected from the scientific literature to tentatively predict the solubility as a function of food composition (water, protein, fat and salt content) and temperature. The best option kept was a random forest algorithm that was used to predict CO2 solubility in four food case studies (ham, salmon, cheese and pâté) that were further used as input parameters in the MAP’ OPT tool, predicting the evolution of headspace gas composition. Predicted CO2 solubilities used as input parameters succeeded in representing the CO2 headspace dynamic as a function of time in the four case studies.

## Related Article

This code was developped in the frame of a published work.


M. Münch, V. Guillard, S. Gaucel, S. Destercke, J. Thévenot, P. Buche.,
_Composition-based statistical model for predicting CO2 solubility in modified atmosphere packaging application_,
Journal of Food Engineering
Volume 340,
2023,
111283,
ISSN 0260-8774,
https://doi.org/10.1016/j.jfoodeng.2022.111283.
